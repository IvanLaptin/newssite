﻿using NewsSite.Data.Model;

namespace NewsSite.Data.Interfaces
{
    public interface IArticleRepository : IRepositoryBase<Article>
    {
        Article GetArticleById(int id);
    }
}
