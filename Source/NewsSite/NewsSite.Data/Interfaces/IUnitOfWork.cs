﻿using Microsoft.EntityFrameworkCore;
using System;
namespace NewsSite.Data.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        DbContext DataContext { get; }
    }
}
