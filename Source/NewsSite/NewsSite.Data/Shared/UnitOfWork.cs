﻿using Microsoft.EntityFrameworkCore;
using NewsSite.Data.Contexts;
using NewsSite.Data.Interfaces;
using System;

namespace NewsSite.Data.Shared
{
    public class UnitOfWork : IUnitOfWork
    {
        private DbContext context;
        private bool disposed = false;

        public UnitOfWork(DbContext context)
        {
            this.context = context as DbContext;
        }

        public DbContext DataContext
        {
            get
            {
                return this.context;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
                this.disposed = true;
            }
        }
    }
}
