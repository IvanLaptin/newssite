﻿using System;
using System.Collections.Generic;

#nullable disable

namespace NewsSite.Data.Model
{
    public partial class Article
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Text { get; set; }
    }
}
