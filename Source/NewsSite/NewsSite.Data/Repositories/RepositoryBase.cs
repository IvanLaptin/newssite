﻿using Microsoft.EntityFrameworkCore;
using NewsSite.Data.Interfaces;

namespace NewsSite.Data.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T>
        where T : class
    {
        protected DbContext DataContext
        {
            get;
            private set;
        }

        public RepositoryBase(IUnitOfWork unitOfWork)
        {
            this.DataContext = unitOfWork.DataContext as DbContext;
        }

        protected DbSet<T> Select()
        {
            return this.DataContext.Set<T>();
        }
    }
}
