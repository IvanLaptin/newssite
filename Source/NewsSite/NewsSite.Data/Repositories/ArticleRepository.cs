﻿using NewsSite.Data.Interfaces;
using NewsSite.Data.Model;
using System.Linq;

namespace NewsSite.Data.Repositories
{
    public class ArticleRepository : RepositoryBase<Article>, IArticleRepository
    {
        public ArticleRepository(IUnitOfWork unitOfWork)
            : base(unitOfWork)
        {

        }

        public Article GetArticleById(int id)
        {
            var article = this.Select().FirstOrDefault(a => a.Id == id);
            return article;
        }
    }
}
