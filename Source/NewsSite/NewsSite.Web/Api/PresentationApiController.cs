﻿using Microsoft.AspNetCore.Mvc;
using NewsSite.Business.Interfaces;
using NewsSite.Web.WebObjects.Responses;

namespace NewsSite.Web.Api
{
    public class PresentationApiController : ControllerBase
    {
        readonly IArticleManager articleManager;
        public PresentationApiController(IArticleManager articleManager)
        {
            this.articleManager = articleManager;
        }

        public GetArticlesResponse GetArticles()
        {
            var res = this.articleManager.GetArticleById(1);
            return null;
        }
    }
}
