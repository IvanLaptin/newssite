﻿using Microsoft.AspNetCore.Mvc;
using NewsSite.Business.Interfaces;

namespace NewsSite.Web.Controllers
{
    public class HomeController : Controller
    {
        readonly IArticleManager articleManager;

        public HomeController(IArticleManager articleManager)
        {
            this.articleManager = articleManager;
        }
        public IActionResult Index()
        {
            var res = this.articleManager.GetArticleById(1);
            return View();
        }
    }
}
