﻿using NewsSite.Model.Dtos;
namespace NewsSite.Business.Interfaces
{
    public interface IArticleManager
    {
        ArticleDto GetArticleById(int id);
    }
}
