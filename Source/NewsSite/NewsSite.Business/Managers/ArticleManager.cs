﻿using NewsSite.Business.Interfaces;
using NewsSite.Data.Interfaces;
using NewsSite.Model.Dtos;

namespace NewsSite.Business.Managers
{
    public class ArticleManager: IArticleManager
    {
        readonly IArticleRepository articleRepository;

        public ArticleManager(IArticleRepository articleRepository)
        {
            this.articleRepository = articleRepository;
        }

        public ArticleDto GetArticleById(int id)
        {
            var article = this.articleRepository.GetArticleById(id);
            ArticleDto articleDto = new ArticleDto()
            {
                Id = article.Id,
                Text = article.Text,
                Title = article.Title
            };
            return articleDto;
        }
    }
}
